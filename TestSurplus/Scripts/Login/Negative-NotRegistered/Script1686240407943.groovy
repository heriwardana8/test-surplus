import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/heri/Downloads/Sample Android App - Login Tes_4.0_Apkpure.apk', true)

//Set Email
Mobile.setText(findTestObject('Object Repository/Mobile/PositifLogin/android.widget.EditText'), 'heriwardana@gmail.com',
	0)

//Set Password
Mobile.setText(findTestObject('Object Repository/Mobile/PositifLogin/android.widget.EditText (1)'), '1234567890', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/Mobile/PositifLogin/android.widget.Button - LOGIN'), 0)

//Validation

//Set Format Name Screenshot
Date today = new Date()

String todaysDate = today.format('ddMMyyyy_HH-mm-ss')

Mobile.takeScreenshot(('/Users/heri/Documents/Katalon/Login/InvalidEmail' + todaysDate) + '.png')

